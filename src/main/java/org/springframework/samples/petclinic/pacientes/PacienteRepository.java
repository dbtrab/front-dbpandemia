package org.springframework.samples.petclinic.pacientes;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.samples.petclinic.PetClinicApplication;
import org.springframework.stereotype.*;
import org.springframework.web.client.RestTemplate;

/**
 * pacienteRepository class is a RESTful wrapper.
 *
 * All method calls are forwarded to a REST server. REST server must be running on
 * localhost.
 *
 * @author marco.mangan@pucrs.br
 *
 */
@Repository
public class PacienteRepository {

	/**
	 * Server-side Application Logger
	 */
	private static final Logger log = Logger.getLogger(PetClinicApplication.class.getName());

	/**
	 * Template for REST requests and responses
	 */
	@Autowired
	RestTemplate restTemplate;

	/**
	 * Creates a request header with username and password.
	 * @param username
	 * @param password
	 * @return a HTTP Basic request header
	 */
	HttpHeaders createHeaders(String username, String password) {
		return new HttpHeaders() {
			/**
			*
			*/
			private static final long serialVersionUID = 1L;

			{
				String auth = username + ":" + password;
				byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")));
				String authHeader = "Basic " + new String(encodedAuth);
				set("Authorization", authHeader);
			}
		};
	}

	/**
	 * Forwards a findById request to the REST server.
	 * @return
	 */
	public Paciente findById(int id) {
		HttpHeaders hs = createHeaders("admin", "admin");

		String serverUri = "http://localhost:9966/petclinic/api/pacientes/{id}";
		HttpEntity<Map<String, Object>> request = new HttpEntity<>(hs);

		ResponseEntity<Paciente> response = this.restTemplate.exchange(serverUri, HttpMethod.GET, request,
				Paciente.class, id);
		if (response.getStatusCode() != HttpStatus.OK) {
			throw new RuntimeException("pacienteRepository::findById ID not found.");
		}
		Paciente paciente = response.getBody();
		log.info(paciente.toString());
		return paciente;
	}

	/**
	 * Forwards a findAll request to the REST server.
	 * @return
	 */
	public List<Paciente> findAll() {
		HttpHeaders hs = createHeaders("admin", "admin");

		String serverUri = String.format("http://localhost:9966/petclinic/api/pacientes");
		HttpEntity<Map<String, Object>> request = new HttpEntity<>(hs);

		ResponseEntity<Paciente[]> response = this.restTemplate.exchange(serverUri, HttpMethod.GET, request,
				Paciente[].class);
		if (response.getStatusCode() != HttpStatus.OK) {
			throw new RuntimeException("pacienteRepository::findAll Resource not found.");
		}
		Paciente[] pacienteArray = response.getBody();
		List<Paciente> pacientes = Arrays.asList(pacienteArray);
		log.info(pacientes.toString());
		return pacientes;
	}

	/**
	 * Forwards a add or update request to the REST server.
	 *
	 * A new paciente object is created on the server and its Id is collected. Otherwise,
	 * the object data is updated on the server.
	 * @param paciente
	 */
	public void save(Paciente paciente) {
		HttpHeaders hs = createHeaders("admin", "admin");
		HttpEntity<Paciente> request = new HttpEntity<>(paciente, hs);
		ResponseEntity<Paciente> response;

		if (paciente.isNew()) {
			String serverUri = "http://localhost:9966/petclinic/api/pacientes";
			response = this.restTemplate.exchange(serverUri, HttpMethod.POST, request, Paciente.class);
			if (response.getStatusCode() != HttpStatus.CREATED) {
				throw new RuntimeException("pacienteRepository::save Save failed.");
			}
			Paciente updated = response.getBody();
			paciente.setId(updated.getId());
		}
		else {
			String serverUri = "http://localhost:9966/petclinic/api/pacientes/{id}";
			response = this.restTemplate.exchange(serverUri, HttpMethod.PUT, request, Paciente.class, paciente.getId());
			if (response.getStatusCode() != HttpStatus.NO_CONTENT) {
				throw new RuntimeException("pacienteRepository::save Update failed.");
			}
		}
		// log.info("::save HTTP Status " + response.getStatusCodeValue());

	}

}
