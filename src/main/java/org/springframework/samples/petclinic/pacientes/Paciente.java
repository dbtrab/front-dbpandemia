package org.springframework.samples.petclinic.pacientes;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;

import org.springframework.samples.petclinic.model.*;
import org.springframework.samples.petclinic.prontuarios.*;

/**
 * Simple JavaBean domain object representing an person.
 *
 * @author Eduarda Keller
 */
public class Paciente extends Pessoa {

	@NotEmpty
	@Digits(fraction = 0, integer = 15)
	protected String cns;

	@NotEmpty
	protected String nomeMae;

	private String obs;

	@NotEmpty
	private String nomeResponsavel;

	@NotEmpty
	@Digits(fraction = 0, integer = 11)
	private String cpfResponsavel;

	@NotEmpty
	@Digits(fraction = 0, integer = 10)
	private String telefoneResponsavel;

	private Prontuario prontuario;

	public void setCns(String cns) {
		this.cns = cns;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public void setNomeResponsavel(String nomeResponsavel) {
		this.nomeResponsavel = nomeResponsavel;
	}

	public void setCpfResponsavel(String cpfResponsavel) {
		this.cpfResponsavel = cpfResponsavel;
	}

	public void setTelefoneResponsavel(String telefoneResponsavel) {
		this.telefoneResponsavel = telefoneResponsavel;
	}

	public String getCns() {
		return cns;
	}

	public String getObs() {
		return obs;
	}

	public String getNomeResponsavel() {
		return nomeResponsavel;
	}

	public String getCpfResponsavel() {
		return cpfResponsavel;
	}

	public String getTelefoneResponsavel() {
		return telefoneResponsavel;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public Prontuario getProntuario() {
		return this.prontuario;
	}

	public void setProntuario(Prontuario prontuario) {
		this.prontuario = prontuario;
	}

}
