package org.springframework.samples.petclinic.pacientes;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author Daniel Cierco
 *
 */

@Controller
public class PacienteController {

	private static final String PACIENTE_LIST = "pacientes/pacienteList";

	private static final String PACIENTE_DETAILS = "pacientes/pacienteDetails";

	private static final String CREATE_OR_UPDATE_PACIENTE_FORM = "pacientes/createOrUpdatePacienteForm";

	@Autowired
	private PacienteRepository pacientes;

	/**
	 * @param model
	 * @return
	 */
	@GetMapping("/pacientes")
	public String showPacientesList(Model model) {
		model.addAttribute("pacientes", pacientes.findAll());
		return PACIENTE_LIST;
	}

	/**
	 * @param model
	 * @return
	 */
	@GetMapping("/pacientes/new")
	public String formpacienteList(Model model) {
		model.addAttribute("paciente", new Paciente());
		return CREATE_OR_UPDATE_PACIENTE_FORM;
	}

	/**
	 * @param paciente
	 * @param result
	 * @return
	 */
	@PostMapping("/pacientes/new")
	public String addpacientesist(@Valid Paciente paciente, BindingResult result) {
		if (result.hasErrors()) {
			return CREATE_OR_UPDATE_PACIENTE_FORM;
		}
		else {
			pacientes.save(paciente);
			return "redirect:/pacientes/" + paciente.getId();
		}
	}

	/**
	 * @param pacienteId
	 * @param model
	 * @return
	 */
	@GetMapping("/pacientes/{pacienteId}/edit")
	public String initUpdatePacienteForm(@PathVariable("pacienteId") int pacienteId, Model model) {
		Paciente paciente = this.pacientes.findById(pacienteId);
		model.addAttribute("paciente", paciente);
		return CREATE_OR_UPDATE_PACIENTE_FORM;
	}

	@PostMapping("/pacientes/{pacienteId}/edit")
	public String processUpdatepacienteForm(@Valid Paciente paciente, BindingResult result,
			@PathVariable("pacienteId") int pacienteId) {
		if (result.hasErrors()) {
			return CREATE_OR_UPDATE_PACIENTE_FORM;
		}
		else {
			paciente.setId(pacienteId);
			this.pacientes.save(paciente);
			return "redirect:/pacientes/{pacienteId}";
		}
	}

	/**
	 * @param pacienteId
	 * @param model
	 * @return
	 */
	@GetMapping("/pacientes/{pacienteId}")
	public String showPaciente(@PathVariable("pacienteId") int pacienteId, Model model) {
		Paciente paciente = this.pacientes.findById(pacienteId);
		model.addAttribute("paciente", paciente);
		return PACIENTE_DETAILS;
	}

}
