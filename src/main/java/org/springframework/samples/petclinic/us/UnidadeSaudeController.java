package org.springframework.samples.petclinic.us;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author Daniel Cierco
 *
 */

@Controller
public class UnidadeSaudeController {

	private static final String UNIDADE_SAUDE_LIST = "unidadesSaude/unidadeSaudeList";

	private static final String UNIDADE_SAUDE_DETAILS = "unidadesSaude/unidadeSaudeDetails";

	private static final String CREATE_OR_UPDATE_UNIDADE_SAUDE_FORM = "unidadesSaude/createOrUpdateUnidadeSaudeForm";

	@Autowired
	private UnidadeSaudeRepository unidadesSaude;

	/**
	 * @param model
	 * @return
	 */
	@GetMapping("/unidadesSaude")
	public String showUnidadesSaudeList(Model model) {
		model.addAttribute("unidadesSaude", unidadesSaude.findAll());
		return UNIDADE_SAUDE_LIST;
	}

	/**
	 * @param model
	 * @return
	 */
	@GetMapping("/unidadesSaude/new")
	public String formUnidadeSaudeList(Model model) {
		model.addAttribute("unidadeSaude", new UnidadeSaude());
		return CREATE_OR_UPDATE_UNIDADE_SAUDE_FORM;
	}

	/**
	 * @param unidadeSaude
	 * @param result
	 * @return
	 */
	@PostMapping("/unidadesSaude/new")
	public String addUnidadesSaudeist(@Valid UnidadeSaude unidadeSaude, BindingResult result) {
		if (result.hasErrors()) {
			return CREATE_OR_UPDATE_UNIDADE_SAUDE_FORM;
		}
		else {
			unidadesSaude.save(unidadeSaude);
			return "redirect:/unidadesSaude/" + unidadeSaude.getId();
		}
	}

	/**
	 * @param unidadeSaudeId
	 * @param model
	 * @return
	 */
	@GetMapping("/unidadesSaude/{unidadeSaudeId}/edit")
	public String initUpdateUnidadeSaudeForm(@PathVariable("unidadeSaudeId") int unidadeSaudeId, Model model) {
		UnidadeSaude unidadeSaude = this.unidadesSaude.findById(unidadeSaudeId);
		model.addAttribute("unidadeSaude", unidadeSaude);
		return CREATE_OR_UPDATE_UNIDADE_SAUDE_FORM;
	}

	@PostMapping("/unidadesSaude/{unidadeSaudeId}/edit")
	public String processUpdateUnidadeSaudeForm(@Valid UnidadeSaude unidadeSaude, BindingResult result,
			@PathVariable("unidadeSaudeId") int unidadeSaudeId) {
		if (result.hasErrors()) {
			return CREATE_OR_UPDATE_UNIDADE_SAUDE_FORM;
		}
		else {
			unidadeSaude.setId(unidadeSaudeId);
			this.unidadesSaude.save(unidadeSaude);
			return "redirect:/unidadesSaude/{unidadeSaudeId}";
		}
	}

	/**
	 * @param unidadeSaudeId
	 * @param model
	 * @return
	 */
	@GetMapping("/unidadesSaude/{unidadeSaudeId}")
	public String showUnidadeSaude(@PathVariable("unidadeSaudeId") int unidadeSaudeId, Model model) {
		UnidadeSaude unidadeSaude = this.unidadesSaude.findById(unidadeSaudeId);
		model.addAttribute("unidadeSaude", unidadeSaude);
		return UNIDADE_SAUDE_DETAILS;
	}

}
