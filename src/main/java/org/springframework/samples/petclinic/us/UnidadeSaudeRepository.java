package org.springframework.samples.petclinic.us;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.samples.petclinic.PetClinicApplication;
import org.springframework.stereotype.*;
import org.springframework.web.client.RestTemplate;

/**
 * UnidadeSaudeRepository class is a RESTful wrapper.
 *
 * All method calls are forwarded to a REST server. REST server must be running on
 * localhost.
 *
 * @author marco.mangan@pucrs.br
 *
 */
@Repository
public class UnidadeSaudeRepository {

	/**
	 * Server-side Application Logger
	 */
	private static final Logger log = Logger.getLogger(PetClinicApplication.class.getName());

	/**
	 * Template for REST requests and responses
	 */
	@Autowired
	RestTemplate restTemplate;

	/**
	 * Creates a request header with username and password.
	 * @param username
	 * @param password
	 * @return a HTTP Basic request header
	 */
	HttpHeaders createHeaders(String username, String password) {
		return new HttpHeaders() {
			/**
			*
			*/
			private static final long serialVersionUID = 1L;

			{
				String auth = username + ":" + password;
				byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")));
				String authHeader = "Basic " + new String(encodedAuth);
				set("Authorization", authHeader);
			}
		};
	}

	/**
	 * Forwards a findById request to the REST server.
	 * @return
	 */
	public UnidadeSaude findById(int id) {
		HttpHeaders hs = createHeaders("admin", "admin");

		String serverUri = "http://localhost:9966/petclinic/api/unidadesSaude/{id}";
		HttpEntity<Map<String, Object>> request = new HttpEntity<>(hs);

		ResponseEntity<UnidadeSaude> response = this.restTemplate.exchange(serverUri, HttpMethod.GET, request,
				UnidadeSaude.class, id);
		if (response.getStatusCode() != HttpStatus.OK) {
			throw new RuntimeException("UnidadeSaudeRepository::findById ID not found.");
		}
		UnidadeSaude unidadeSaude = response.getBody();
		log.info(unidadeSaude.toString());
		return unidadeSaude;
	}

	/**
	 * Forwards a findAll request to the REST server.
	 * @return
	 */
	public List<UnidadeSaude> findAll() {
		HttpHeaders hs = createHeaders("admin", "admin");

		String serverUri = String.format("http://localhost:9966/petclinic/api/unidadesSaude");
		HttpEntity<Map<String, Object>> request = new HttpEntity<>(hs);

		ResponseEntity<UnidadeSaude[]> response = this.restTemplate.exchange(serverUri, HttpMethod.GET, request,
				UnidadeSaude[].class);
		if (response.getStatusCode() != HttpStatus.OK) {
			throw new RuntimeException("UnidadeSaudeRepository::findAll Resource not found.");
		}
		UnidadeSaude[] unidadeSaudeArray = response.getBody();
		List<UnidadeSaude> unidadesSaude = Arrays.asList(unidadeSaudeArray);
		log.info(unidadesSaude.toString());
		return unidadesSaude;
	}

	/**
	 * Forwards a add or update request to the REST server.
	 *
	 * A new UnidadeSaude object is created on the server and its Id is collected.
	 * Otherwise, the object data is updated on the server.
	 * @param UnidadeSaude
	 */
	public void save(UnidadeSaude UnidadeSaude) {
		HttpHeaders hs = createHeaders("admin", "admin");
		HttpEntity<UnidadeSaude> request = new HttpEntity<>(UnidadeSaude, hs);
		ResponseEntity<UnidadeSaude> response;

		if (UnidadeSaude.isNew()) {
			String serverUri = "http://localhost:9966/petclinic/api/unidadesSaude";
			response = this.restTemplate.exchange(serverUri, HttpMethod.POST, request, UnidadeSaude.class);
			if (response.getStatusCode() != HttpStatus.CREATED) {
				throw new RuntimeException("UnidadeSaudeRepository::save Save failed.");
			}
			UnidadeSaude updated = response.getBody();
			UnidadeSaude.setId(updated.getId());
		}
		else {
			String serverUri = "http://localhost:9966/petclinic/api/unidadesSaude/{id}";
			response = this.restTemplate.exchange(serverUri, HttpMethod.PUT, request, UnidadeSaude.class,
					UnidadeSaude.getId());
			if (response.getStatusCode() != HttpStatus.NO_CONTENT) {
				throw new RuntimeException("UnidadeSaudeRepository::save Update failed.");
			}
		}
		// log.info("::save HTTP Status " + response.getStatusCodeValue());

	}

}
