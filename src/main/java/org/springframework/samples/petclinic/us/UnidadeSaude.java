
package org.springframework.samples.petclinic.us;

import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.Collections;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;

import org.springframework.samples.petclinic.funcionarios.*;
import org.springframework.samples.petclinic.model.BaseEntity;

import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Simple JavaBean domain object representing an person.
 *
 * @author Daniel Cierco
 */
public class UnidadeSaude extends BaseEntity {

	@NotEmpty
	protected String nome;

	@NotEmpty
	private String endereco;

	@NotEmpty
	@Digits(fraction = 0, integer = 8)
	private String cep;

	@NotEmpty
	@Digits(fraction = 0, integer = 10)
	private String telefone;

	private Set<Funcionario> funcionarios;

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return this.endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getCep() {
		return this.cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getTelefone() {
		return this.telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	@JsonIgnore
	protected Set<Funcionario> getFuncionariosInternal() {
		if (this.funcionarios == null) {
			this.funcionarios = new HashSet<>();
		}
		return this.funcionarios;
	}

	protected void setFuncionariosInternal(Set<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}

	public void addFuncionario(Funcionario funcionario) {
		getFuncionariosInternal().add(funcionario);
		funcionario.setUnidadeSaude(this);
	}

	public List<Funcionario> getFuncionarios() {
		List<Funcionario> sortedFuncionarios = new ArrayList<>(getFuncionariosInternal());
		PropertyComparator.sort(sortedFuncionarios, new MutableSortDefinition("nome", true, true));
		return Collections.unmodifiableList(sortedFuncionarios);
	}

	/**
	 * Return the Funcionario with the given name, or null if none found for this
	 * UnidadeSaude.
	 * @param name to test
	 * @return true if funcionario name is already in use
	 */
	public Funcionario getFuncionario(String name) {
		return getFuncionario(name, false);
	}

	/**
	 * Return the Funcionario with the given name, or null if none found for this
	 * UnidadeSaude.
	 * @param name to test
	 * @return true if Funcionario name is already in use
	 */
	public Funcionario getFuncionario(String name, boolean ignoreNew) {
		name = name.toLowerCase();
		for (Funcionario funcionario : getFuncionariosInternal()) {
			if (!ignoreNew || !funcionario.isNew()) {
				String compName = funcionario.getNomeCompleto();
				compName = compName.toLowerCase();
				if (compName.equals(name)) {
					return funcionario;
				}
			}
		}
		return null;
	}

}
