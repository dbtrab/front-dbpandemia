package org.springframework.samples.petclinic.funcionarios;

import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.Collections;

import org.springframework.samples.petclinic.us.*;
import org.springframework.samples.petclinic.model.*;
import org.springframework.samples.petclinic.prontuarios.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;
import javax.validation.constraints.NotEmpty;

/**
 * Simple JavaBean domain object representing an person.
 *
 * @author Eduarda Keller
 */
public class Funcionario extends Pessoa {

	@NotEmpty
	protected String setor;

	@NotEmpty
	protected String funcao;

	private UnidadeSaude unidadeSaude;

	private Set<Prontuario> prontuarios;

	public String getSetor() {
		return setor;
	}

	public String getFuncao() {
		return funcao;
	}

	public UnidadeSaude getUnidadeSaude() {
		return this.unidadeSaude;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

	public void setUnidadeSaude(UnidadeSaude unidadeSaude) {
		this.unidadeSaude = unidadeSaude;
	}

	@JsonIgnore
	protected Set<Prontuario> getProntuariosInternal() {
		if (this.prontuarios == null) {
			this.prontuarios = new HashSet<>();
		}
		return this.prontuarios;
	}

	protected void setProntuariosInternal(Set<Prontuario> prontuarios) {
		this.prontuarios = prontuarios;
	}

	public void addProntuario(Prontuario prontuario) {
		getProntuariosInternal().add(prontuario);
		prontuario.setFuncionario(this);
	}

	public List<Prontuario> getProntuarios() {
		List<Prontuario> sortedProntuarios = new ArrayList<>(getProntuariosInternal());
		PropertyComparator.sort(sortedProntuarios, new MutableSortDefinition("nome", true, true));
		return Collections.unmodifiableList(sortedProntuarios);
	}

	/**
	 * Return the Funcionario with the given name, or null if none found for this
	 * UnidadeSaude.
	 * @param nome to test
	 * @return true if prontuario nome is already in use
	 */
	public Prontuario getProntuario(String nome) {
		return getProntuario(nome, false);
	}

	/**
	 * Return the Funcionario with the given name, or null if none found for this
	 * UnidadeSaude.
	 * @param name to test
	 * @return true if Funcionario name is already in use
	 */
	public Prontuario getProntuario(String nome, boolean ignoreNew) {
		nome = nome.toLowerCase();
		for (Prontuario prontuario : getProntuariosInternal()) {
			if (!ignoreNew || !prontuario.isNew()) {
				String compName = prontuario.getPaciente().getNomeCompleto();
				compName = compName.toLowerCase();
				if (compName.equals(nome)) {
					return prontuario;
				}
			}
		}
		return null;
	}

}
