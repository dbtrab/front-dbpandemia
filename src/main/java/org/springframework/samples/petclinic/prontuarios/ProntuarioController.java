package org.springframework.samples.petclinic.prontuarios;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author Daniel Cierco
 *
 */

@Controller
public class ProntuarioController {

	private static final String PRONTUARIO_LIST = "prontuarios/prontuarioList";

	private static final String PRONTUARIO_DETAILS = "prontuarios/prontuarioDetails";

	private static final String CREATE_OR_UPDATE_PRONTUARIO_FORM = "prontuarios/createOrUpdateProntuarioForm";

	@Autowired
	private ProntuarioRepository prontuarios;

	/**
	 * @param model
	 * @return
	 */
	@GetMapping("/prontuarios")
	public String showProntuariosList(Model model) {
		model.addAttribute("prontuarios", prontuarios.findAll());
		return PRONTUARIO_LIST;
	}

	/**
	 * @param model
	 * @return
	 */
	@GetMapping("/prontuarios/new")
	public String formprontuarioList(Model model) {
		model.addAttribute("prontuario", new Prontuario());
		return CREATE_OR_UPDATE_PRONTUARIO_FORM;
	}

	/**
	 * @param prontuario
	 * @param result
	 * @return
	 */
	@PostMapping("/prontuarios/new")
	public String addprontuariosist(@Valid Prontuario prontuario, BindingResult result) {
		if (result.hasErrors()) {
			return CREATE_OR_UPDATE_PRONTUARIO_FORM;
		}
		else {
			prontuarios.save(prontuario);
			return "redirect:/prontuarios/" + prontuario.getId();
		}
	}

	/**
	 * @param prontuarioId
	 * @param model
	 * @return
	 */
	@GetMapping("/prontuarios/{prontuarioId}/edit")
	public String initUpdateprontuarioForm(@PathVariable("prontuarioId") int prontuarioId, Model model) {
		Prontuario prontuario = this.prontuarios.findById(prontuarioId);
		model.addAttribute("prontuario", prontuario);
		return CREATE_OR_UPDATE_PRONTUARIO_FORM;
	}

	@PostMapping("/prontuarios/{prontuarioId}/edit")
	public String processUpdateprontuarioForm(@Valid Prontuario prontuario, BindingResult result,
			@PathVariable("prontuarioId") int prontuarioId) {
		if (result.hasErrors()) {
			return CREATE_OR_UPDATE_PRONTUARIO_FORM;
		}
		else {
			prontuario.setId(prontuarioId);
			this.prontuarios.save(prontuario);
			return "redirect:/prontuarios/{prontuarioId}";
		}
	}

	/**
	 * @param prontuarioId
	 * @param model
	 * @return
	 */
	@GetMapping("/prontuarios/{prontuarioId}")
	public String showprontuario(@PathVariable("prontuarioId") int prontuarioId, Model model) {
		Prontuario prontuario = this.prontuarios.findById(prontuarioId);
		model.addAttribute("prontuario", prontuario);
		return PRONTUARIO_DETAILS;
	}

}
