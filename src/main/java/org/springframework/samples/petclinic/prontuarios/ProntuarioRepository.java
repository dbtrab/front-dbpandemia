package org.springframework.samples.petclinic.prontuarios;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.samples.petclinic.PetClinicApplication;
import org.springframework.stereotype.*;
import org.springframework.web.client.RestTemplate;

/**
 * prontuarioRepository class is a RESTful wrapper.
 *
 * All method calls are forwarded to a REST server. REST server must be running on
 * localhost.
 *
 * @author marco.mangan@pucrs.br
 *
 */
@Repository
public class ProntuarioRepository {

	/**
	 * Server-side Application Logger
	 */
	private static final Logger log = Logger.getLogger(PetClinicApplication.class.getName());

	/**
	 * Template for REST requests and responses
	 */
	@Autowired
	RestTemplate restTemplate;

	/**
	 * Creates a request header with username and password.
	 * @param username
	 * @param password
	 * @return a HTTP Basic request header
	 */
	HttpHeaders createHeaders(String username, String password) {
		return new HttpHeaders() {
			/**
			*
			*/
			private static final long serialVersionUID = 1L;

			{
				String auth = username + ":" + password;
				byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")));
				String authHeader = "Basic " + new String(encodedAuth);
				set("Authorization", authHeader);
			}
		};
	}

	/**
	 * Forwards a findById request to the REST server.
	 * @return
	 */
	public Prontuario findById(int id) {
		HttpHeaders hs = createHeaders("admin", "admin");

		String serverUri = "http://localhost:9966/petclinic/api/prontuarios/{id}";
		HttpEntity<Map<String, Object>> request = new HttpEntity<>(hs);

		ResponseEntity<Prontuario> response = this.restTemplate.exchange(serverUri, HttpMethod.GET, request,
				Prontuario.class, id);
		if (response.getStatusCode() != HttpStatus.OK) {
			throw new RuntimeException("prontuarioRepository::findById ID not found.");
		}
		Prontuario prontuario = response.getBody();
		log.info(prontuario.toString());
		return prontuario;
	}

	/**
	 * Forwards a findAll request to the REST server.
	 * @return
	 */
	public List<Prontuario> findAll() {
		HttpHeaders hs = createHeaders("admin", "admin");

		String serverUri = String.format("http://localhost:9966/petclinic/api/prontuarios");
		HttpEntity<Map<String, Object>> request = new HttpEntity<>(hs);

		ResponseEntity<Prontuario[]> response = this.restTemplate.exchange(serverUri, HttpMethod.GET, request,
				Prontuario[].class);
		if (response.getStatusCode() != HttpStatus.OK) {
			throw new RuntimeException("prontuarioRepository::findAll Resource not found.");
		}
		Prontuario[] prontuarioArray = response.getBody();
		List<Prontuario> prontuarios = Arrays.asList(prontuarioArray);
		log.info(prontuarios.toString());
		return prontuarios;
	}

	/**
	 * Forwards a add or update request to the REST server.
	 *
	 * A new prontuario object is created on the server and its Id is collected.
	 * Otherwise, the object data is updated on the server.
	 * @param prontuario
	 */
	public void save(Prontuario prontuario) {
		HttpHeaders hs = createHeaders("admin", "admin");
		HttpEntity<Prontuario> request = new HttpEntity<>(prontuario, hs);
		ResponseEntity<Prontuario> response;

		if (prontuario.isNew()) {
			String serverUri = "http://localhost:9966/petclinic/api/prontuarios";
			response = this.restTemplate.exchange(serverUri, HttpMethod.POST, request, Prontuario.class);
			if (response.getStatusCode() != HttpStatus.CREATED) {
				throw new RuntimeException("prontuarioRepository::save Save failed.");
			}
			Prontuario updated = response.getBody();
			prontuario.setId(updated.getId());
		}
		else {
			String serverUri = "http://localhost:9966/petclinic/api/prontuarios/{id}";
			response = this.restTemplate.exchange(serverUri, HttpMethod.PUT, request, Prontuario.class,
					prontuario.getId());
			if (response.getStatusCode() != HttpStatus.NO_CONTENT) {
				throw new RuntimeException("prontuarioRepository::save Update failed.");
			}
		}
		// log.info("::save HTTP Status " + response.getStatusCodeValue());

	}

}
