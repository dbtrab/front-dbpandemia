package org.springframework.samples.petclinic.prontuarios;

import org.springframework.samples.petclinic.model.*;
import org.springframework.samples.petclinic.pacientes.*;
import org.springframework.samples.petclinic.funcionarios.*;
import org.springframework.samples.petclinic.testes.*;

import java.util.Date;
import java.util.Set;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * Simple JavaBean domain object representing an person.
 *
 * @author Eduarda Keller
 */
public class Prontuario extends BaseEntity {

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date data;

	private Paciente paciente;

	private Funcionario funcionario;

	private Set<Teste> testes;

	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Paciente getPaciente() {
		return this.paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Funcionario getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

}
