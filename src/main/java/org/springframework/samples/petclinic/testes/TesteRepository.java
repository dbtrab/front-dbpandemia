package org.springframework.samples.petclinic.testes;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.samples.petclinic.PetClinicApplication;
import org.springframework.stereotype.*;
import org.springframework.web.client.RestTemplate;

/**
 * @author Eduarda Keller
 *
 */

@Repository
public class TesteRepository {

	/**
	 * Server-side Application Logger
	 */
	private static final Logger log = Logger.getLogger(PetClinicApplication.class.getName());

	/**
	 * Template for REST requests and responses
	 */
	@Autowired
	RestTemplate restTemplate;

	/**
	 * Creates a request header with username and password.
	 * @param username
	 * @param password
	 * @return a HTTP Basic request header
	 */
	HttpHeaders createHeaders(String username, String password) {
		return new HttpHeaders() {
			/**
			*
			*/
			private static final long serialVersionUID = 1L;

			{
				String auth = username + ":" + password;
				byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")));
				String authHeader = "Basic " + new String(encodedAuth);
				set("Authorization", authHeader);
			}
		};
	}

	/**
	 * Forwards a findById request to the REST server.
	 * @return
	 */
	public Teste findById(int id) {
		HttpHeaders hs = createHeaders("admin", "admin");

		String serverUri = "http://localhost:9966/petclinic/api/testes/{id}";
		HttpEntity<Map<String, Object>> request = new HttpEntity<>(hs);

		ResponseEntity<Teste> response = this.restTemplate.exchange(serverUri, HttpMethod.GET, request, Teste.class,
				id);
		if (response.getStatusCode() != HttpStatus.OK) {
			throw new RuntimeException("TesteRepository::findById ID not found.");
		}
		Teste teste = response.getBody();
		log.info(teste.toString());
		return teste;
	}

	/**
	 * Forwards a findAll request to the REST server.
	 * @return
	 */
	public List<Teste> findAll() {
		HttpHeaders hs = createHeaders("admin", "admin");

		String serverUri = String.format("http://localhost:9966/petclinic/api/testes");
		HttpEntity<Map<String, Object>> request = new HttpEntity<>(hs);

		ResponseEntity<Teste[]> response = this.restTemplate.exchange(serverUri, HttpMethod.GET, request,
				Teste[].class);
		if (response.getStatusCode() != HttpStatus.OK) {
			throw new RuntimeException("TesteRepository::findAll Resource not found.");
		}
		Teste[] testeArray = response.getBody();
		List<Teste> teste = Arrays.asList(testeArray);
		log.info(teste.toString());
		return teste;
	}

	/**
	 * Forwards a add or update request to the REST server.
	 *
	 * A new Teste object is created on the server and its Id is collected. Otherwise, the
	 * object data is updated on the server.
	 * @param Teste
	 */
	public void save(Teste Teste) {
		HttpHeaders hs = createHeaders("admin", "admin");
		HttpEntity<Teste> request = new HttpEntity<>(Teste, hs);
		ResponseEntity<Teste> response;

		if (Teste.isNew()) {
			String serverUri = "http://localhost:9966/petclinic/api/testes";
			response = this.restTemplate.exchange(serverUri, HttpMethod.POST, request, Teste.class);
			if (response.getStatusCode() != HttpStatus.CREATED) {
				throw new RuntimeException("TesteRepository::save Save failed.");
			}
			Teste updated = response.getBody();
			Teste.setId(updated.getId());
		}
		else {
			String serverUri = "http://localhost:9966/petclinic/api/testes/{id}";
			response = this.restTemplate.exchange(serverUri, HttpMethod.PUT, request, Teste.class, Teste.getId());
			if (response.getStatusCode() != HttpStatus.NO_CONTENT) {
				throw new RuntimeException("TesteRepository::save Update failed.");
			}
		}
		// log.info("::save HTTP Status " + response.getStatusCodeValue());

	}

}
