package org.springframework.samples.petclinic.testes;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.samples.petclinic.PetClinicApplication;
import org.springframework.web.client.RestTemplate;

/**
 * @author Eduarda Keller
 *
 */

public class TipoTesteRepository {

	/**
	 * Server-side Application Logger
	 */
	private static final Logger log = Logger.getLogger(PetClinicApplication.class.getName());

	/**
	 * Template for REST requests and responses
	 */
	@Autowired
	RestTemplate restTemplate;

	/**
	 * Creates a request header with username and password.
	 * @param username
	 * @param password
	 * @return a HTTP Basic request header
	 */
	HttpHeaders createHeaders(String username, String password) {
		return new HttpHeaders() {
			/**
			*
			*/
			private static final long serialVersionUID = 1L;

			{
				String auth = username + ":" + password;
				byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")));
				String authHeader = "Basic " + new String(encodedAuth);
				set("Authorization", authHeader);
			}
		};
	}

	/**
	 * Forwards a findById request to the REST server.
	 * @return
	 */
	public TipoTeste findById(int id) {
		HttpHeaders hs = createHeaders("admin", "admin");

		String serverUri = "http://localhost:9966/petclinic/api/tipoTestes/{id}";
		HttpEntity<Map<String, Object>> request = new HttpEntity<>(hs);

		ResponseEntity<TipoTeste> response = this.restTemplate.exchange(serverUri, HttpMethod.GET, request,
				TipoTeste.class, id);
		if (response.getStatusCode() != HttpStatus.OK) {
			throw new RuntimeException("TipoTesteRepository::findById ID not found.");
		}
		TipoTeste tipo = response.getBody();
		log.info(tipo.toString());
		return tipo;
	}

	/**
	 * Forwards a findAll request to the REST server.
	 * @return
	 */
	public List<TipoTeste> findAll() {
		HttpHeaders hs = createHeaders("admin", "admin");

		String serverUri = String.format("http://localhost:9966/petclinic/api/tipoTestes");
		HttpEntity<Map<String, Object>> request = new HttpEntity<>(hs);

		ResponseEntity<TipoTeste[]> response = this.restTemplate.exchange(serverUri, HttpMethod.GET, request,
				TipoTeste[].class);
		if (response.getStatusCode() != HttpStatus.OK) {
			throw new RuntimeException("TipoTesteRepository::findAll Resource not found.");
		}
		TipoTeste[] tipoArray = response.getBody();
		List<TipoTeste> tipo = Arrays.asList(tipoArray);
		log.info(tipo.toString());
		return tipo;
	}

	/**
	 * Forwards a add or update request to the REST server.
	 *
	 * A new TipoTeste object is created on the server and its Id is collected. Otherwise,
	 * the object data is updated on the server.
	 * @param Teste
	 */

	public void save(TipoTeste TipoTeste) {
		HttpHeaders hs = createHeaders("admin", "admin");
		HttpEntity<TipoTeste> request = new HttpEntity<>(TipoTeste, hs);
		ResponseEntity<TipoTeste> response;

		if (TipoTeste.isNew()) {
			String serverUri = "http://localhost:9966/petclinic/api/tipoTestes";
			response = this.restTemplate.exchange(serverUri, HttpMethod.POST, request, TipoTeste.class);
			if (response.getStatusCode() != HttpStatus.CREATED) {
				throw new RuntimeException("TesteRepository::save Save failed.");
			}
			TipoTeste updated = response.getBody();
			TipoTeste.setId(updated.getId());
		}
		else {
			String serverUri = "http://localhost:9966/petclinic/api/tipoTestes/{id}";
			response = this.restTemplate.exchange(serverUri, HttpMethod.PUT, request, TipoTeste.class,
					TipoTeste.getId());
			if (response.getStatusCode() != HttpStatus.NO_CONTENT) {
				throw new RuntimeException("TesteRepository::save Update failed.");
			}
		}
		// log.info("::save HTTP Status " + response.getStatusCodeValue());

	}

}
