package org.springframework.samples.petclinic.testes;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author Eduarda Keller
 *
 */

public class TipoTesteController {

	private static final String TIPO_TESTE_LIST = "testes/tipoTesteList";

	private static final String TIPO_TESTE_DETAILS = "testes/tipoTesteDetails";

	private static final String CREATE_OR_UPDATE_TIPO_TESTE_FORM = "testes/createOrUpdateTipoTesteForm";

	@Autowired
	private TipoTesteRepository tipo;

	/**
	 * @param model
	 * @return
	 */
	@GetMapping("/tipoTestes")
	public String showTipotesteList(Model model) {
		model.addAttribute("tipo", tipo.findAll());
		return TIPO_TESTE_LIST;
	}

	/**
	 * @param model
	 * @return
	 */
	@GetMapping("/tipoTestes/new")
	public String formTipoList(Model model) {
		model.addAttribute("tipo", new TipoTeste());
		return CREATE_OR_UPDATE_TIPO_TESTE_FORM;
	}

	/**
	 * @param tipo
	 * @param result
	 * @return
	 */
	@PostMapping("/tipoTestes/new")
	public String addTipoList(@Valid TipoTeste tipo, BindingResult result) {
		if (result.hasErrors()) {
			return CREATE_OR_UPDATE_TIPO_TESTE_FORM;
		}
		else {
			this.tipo.save(tipo);
			return "redirect:/tipo/" + tipo.getId();
		}
	}

	/**
	 * @param tipoId
	 * @param model
	 * @return
	 */
	@GetMapping("/tipoTestes/{tipoId}/edit")
	public String initUpdateTipoForm(@PathVariable("tipoId") int tipoId, Model model) {
		TipoTeste tipo = this.tipo.findById(tipoId);
		model.addAttribute("tipo", tipo);
		return CREATE_OR_UPDATE_TIPO_TESTE_FORM;
	}

	@PostMapping("/tipoTestes/{tipoId}/edit")
	public String processUpdateTipoForm(@Valid TipoTeste tipo, BindingResult result,
			@PathVariable("tipoId") int tipoId) {
		if (result.hasErrors()) {
			return CREATE_OR_UPDATE_TIPO_TESTE_FORM;
		}
		else {
			tipo.setId(tipoId);
			this.tipo.save(tipo);
			return "redirect:/tipo/{tipoId}";
		}
	}

	/**
	 * @param tipoId
	 * @param model
	 * @return
	 */
	@GetMapping("/tipoTestes/{tipoId}")
	public String showTipoTeste(@PathVariable("tipoId") int tipoId, Model model) {
		TipoTeste tipo = this.tipo.findById(tipoId);
		model.addAttribute("tipo", tipo);
		return TIPO_TESTE_DETAILS;
	}

}
