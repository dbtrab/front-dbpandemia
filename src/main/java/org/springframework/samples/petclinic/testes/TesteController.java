package org.springframework.samples.petclinic.testes;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author Eduarda Keller
 *
 */

@Controller
public class TesteController {

	private static final String TESTE_LIST = "testes/tipoTesteList";

	private static final String TESTE_DETAILS = "testes/testeDetails";

	private static final String CREATE_OR_UPDATE_TESTE_FORM = "testes/createOrUpdateTesteForm";

	@Autowired
	private TesteRepository teste;

	/**
	 * @param model
	 * @return
	 */
	@GetMapping("/teste")
	public String showTesteList(Model model) {
		model.addAttribute("teste", teste.findAll());
		return TESTE_LIST;
	}

	/**
	 * @param model
	 * @return
	 */
	@GetMapping("/teste/new")
	public String formTesteList(Model model) {
		model.addAttribute("teste", new Teste());
		return CREATE_OR_UPDATE_TESTE_FORM;
	}

	/**
	 * @param teste
	 * @param result
	 * @return
	 */
	@PostMapping("/teste/new")
	public String addTesteList(@Valid Teste teste, BindingResult result) {
		if (result.hasErrors()) {
			return CREATE_OR_UPDATE_TESTE_FORM;
		}
		else {
			this.teste.save(teste);
			return "redirect:/teste/" + teste.getId();
		}
	}

	/**
	 * @param testeId
	 * @param model
	 * @return
	 */
	@GetMapping("/teste/{testeId}/edit")
	public String initUpdateTesteForm(@PathVariable("testeId") int testeId, Model model) {
		Teste teste = this.teste.findById(testeId);
		model.addAttribute("teste", teste);
		return CREATE_OR_UPDATE_TESTE_FORM;
	}

	@PostMapping("/teste/{testeId}/edit")
	public String processUpdateTesteForm(@Valid Teste teste, BindingResult result,
			@PathVariable("testeId") int testeId) {
		if (result.hasErrors()) {
			return CREATE_OR_UPDATE_TESTE_FORM;
		}
		else {
			teste.setId(testeId);
			this.teste.save(teste);
			return "redirect:/teste/{testeId}";
		}
	}

	/**
	 * @param testeId
	 * @param model
	 * @return
	 */
	@GetMapping("/teste/{testeId}")
	public String showTeste(@PathVariable("testeId") int testeId, Model model) {
		Teste teste = this.teste.findById(testeId);
		model.addAttribute("teste", teste);
		return TESTE_DETAILS;
	}

}
