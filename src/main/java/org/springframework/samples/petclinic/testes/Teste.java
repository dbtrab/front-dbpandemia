package org.springframework.samples.petclinic.testes;

import org.springframework.samples.petclinic.model.*;
import org.springframework.samples.petclinic.prontuarios.*;

import javax.validation.constraints.NotEmpty;

/**
 * Simple JavaBean domain object representing an person.
 *
 * @author Daniel Cierco
 */
public class Teste extends BaseEntity {

	@NotEmpty
	protected String resultado;

	private TipoTeste tipo;

	private Prontuario prontuario;

	public Prontuario getProntuario() {
		return this.prontuario;
	}

	public void setProntuario(Prontuario prontuario) {
		this.prontuario = prontuario;
	}

	public TipoTeste getTipo() {
		return this.tipo;
	}

	public void setTipo(TipoTeste tipo) {
		this.tipo = tipo;
	}

	public String getResultado() {
		return this.resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

}
